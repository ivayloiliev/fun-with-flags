let nafArr = ['cerwyn', 'forrester', 'hornwood','karstark','mormont','stark','umber','whitehill'];
const NAF = 'notflag';

var $img = $('.img-container');
$img.on('click', function(){
    checkQuestion($(this).attr('id'));
    changeQuestion();
});

function moveCoin() {
    if (currentRound == rounds[0] || currentRound == rounds[1]) {
        moveit();
    }
    if ((currentRound == roundsForRed[0] || currentRound == roundsForRed[1]) && roundsForRed[0] != rounds[0] &&
        roundsForRed[0] != rounds[1] && roundsForRed[1] != rounds[0] && roundsForRed[1] != rounds[1]) {
        // moveitRed();
    }
}

function loadNextQuestion() {
    $('.not-a-flag').css({'pointer-events':'auto','text-decoration':'none'});
    var $label = $('.country-name');
    var question = questions[currentRound];
    clearInterval(interval);
    setTimer();
    console.log(questions);
    $label.text(question.label);
    $img.each(function (index, image) {
        var i = index + 1;
        var countryTag = String(question['a' + i]).toLowerCase();
        if (countryTag === NAF) {
            countryTag = nafArr[Math.floor(Math.random(nafArr.length))];
            $(image).css('background-image', ['url("', 'images/flags/', countryTag, '.png")'].join(''))
        }
        $(image).css('background-image', ['url("', 'images/flags/', countryTag, '.png")'].join(''))
        $(image).attr('id', countryTag);
    });
}
function sendUserStatusEvent() {
    var changeUserStatus = {
        changeUserStatus: {
            userId: window.userId,
            status: currentRound,
            sessionId: window._sessionId
        }
    };
    sendEvent(changeUserStatus);
}
function changeQuestion(){
    currentRound++;
    movePlayer(userId, currentRound);
    sendUserStatusEvent();
    if(currentRound == questions.length) {
        gameOver();
        return;
    }
    $('#question-count').html('Round <span class="color=-red">' + (currentRound+1) + '</span>/' + questions.length);
    $('#player-score').html(playerScore);
    moveCoin();
    loadNextQuestion();


    // movePlayer(userId, currentRound);
    // currentRound++;
    //
    // if(currentRound == questions.length) {
    //   gameOver();
    //   return;
    // }







}

$('body').on('keydown', function (event) {
    if(getActiveScreen() != 'game-room') return;
    if(isScreenFrozen) return;

    var flags = new Array;

    $('.img-container').each(function () {
        flags.push($(this).attr('id'));
    });


    switch(event.which) {
        case 49: //key 1
            checkQuestion(flags[0]);
            changeQuestion();
            break;
        case 50: //key 2
            checkQuestion(flags[1]);
            changeQuestion();
            break;
        case 51: //key 3
            checkQuestion(flags[2]);
            changeQuestion();
            break;
        case 52: //key 4
            checkQuestion(flags[3]);
            changeQuestion();
            break;
        case 53: //key 5
            checkQuestion(flags[4]);
            changeQuestion();
            break;

        case 81: //key q
            console.log("pressed: " + flags[0]);
            notAFlag('#' + flags[0]);
            break;

        case 87: //key w
            console.log("pressed: " + flags[1]);
            notAFlag('#' + flags[1]);
            break;

        case 69: //key e
            console.log("pressed: " + flags[2]);
            notAFlag('#' + flags[2]);
            break;

        case 82: //key r
            console.log("pressed: " + flags[3]);
            notAFlag('#' + flags[3]);
            break;

        case 84: //key t
            console.log("pressed: " + flags[4]);
            notAFlag('#' + flags[4]);
            break;
        default: break;
    }

    console.log(event.which);
});

    returnCoinToStartPosition();
    returnCoinToStartPositionRed();

    function moveit(){
      var width = $(document).width();
      var random1 = Math.floor((Math.random() * width) + 1);
      var random2 = Math.floor((Math.random() * width) + 1);
      var randomizedHeight = $(document).height();
      while(random1 == random2){
        random2 = Math.floor((Math.random() * width) + 1);
      }

      $('.coin').show().animate({
        'top' : randomizedHeight,
        'left' : random2
      }, 5000, returnCoinToStartPosition);
    }

    function returnCoinToStartPosition(random){
      var width = $(document).width();
      $('.coin').hide();
      $('.coin').css({
        'top': '-30px',
        'left': Math.floor((Math.random() * width) + 1)
      });
    }

    $('.coin').on('click', function(){
      var scoreToAdd = Math.floor((Math.random() * 11) + 5);
      playerScore += scoreToAdd;
      $('#player-score').html(playerScore);
      $(this).hide();
    });

    function moveitRed(){
      var width = $(document).width();
      var random1 = Math.floor((Math.random() * width) + 1);
      var random2 = Math.floor((Math.random() * width) + 1);
      var randomizedHeight = $(document).height();
      while(random1 == random2){
        random2 = Math.floor((Math.random() * width) + 1);
      }

      $('.coin_red').show().animate({
        'top' : randomizedHeight,
        'left' : random2
      }, 5000, returnCoinToStartPositionRed);
    }

    function returnCoinToStartPositionRed(random){
      var width = $(document).width();
      $('.coin_red').hide();
      $('.coin_red').css({
        'top': '-30px',
        'left': Math.floor((Math.random() * width) + 1)
      });
    }

    $('.coin_red').on('click', function(event){
      $(this).hide();
      var scoreToSub = Math.floor((Math.random() * 11) + 5);
      playerScore -= scoreToSub;
      if (playerScore < 0){
            playerScore = 0;
      }
    });

    function setRound(){
    var firstRound = Math.floor((Math.random() * 10) + 1);
    var secondRound = Math.floor((Math.random() * 9) + 1);
    while(firstRound == secondRound){
      secondRound = Math.floor((Math.random() * 9) + 1);;
    }
    rounds[0] = firstRound;
    rounds[1] = secondRound;
    }

        function setRoundRed(){
    var firstRound = Math.floor((Math.random() * 10) + 1);
    var secondRound = Math.floor((Math.random() * 9) + 1);
    while(firstRound == secondRound){
      secondRound = Math.floor((Math.random() * 9) + 1);;
    }
    roundsForRed[0] = firstRound;
    roundsForRed[1] = secondRound;
    }

    setRound();
    setRoundRed();
