jQuery( document ).ready(function() {
    connection = new WebSocket('ws://10.191.39.30:9999');
    connection.onopen = function () {
      connection.send('Ping'); // Send the message 'Ping' to the server
      hideLoadingScreen();
      changeScreenTo('login-page ');
    };

    // Log errors
    connection.onerror = function (error) {
      console.log('WebSocket Error ' + error);
    };

    connection.addEventListener('close', function() {
        sendLeaveRoom();
    });

    $(window).on('beforeunload', function () {
        sendLeaveRoom();
    });

    // Log messages from the server
    connection.onmessage = function (e) {
      messageProcess(e.data);
      console.log('Server: ' + e.data);
    };

    //Connection closed
    connection.addEventListener('close', function(){
      expose.DeactivateConfetti();
      changeScreenTo('connection-close-popup');
    });

});
function sendEvent(object){
  var json_data = JSON.stringify(object);
  console.log('Sending: ' + json_data);
  connection.send(json_data);
}

function messageProcess(receivedData){
  if(receivedData == 'Ping') return;
  try{
    var receivedData = jQuery.parseJSON(receivedData);
    if(receivedData.hasOwnProperty('roomMembers')){
        if (gameStarted) {
            return;
        }
        onRoomCreated(receivedData);
    } else if(receivedData.hasOwnProperty('questions')){
        if (gameStarted) {
            return;
        }
        questions = receivedData.questions;
        _sessionId = receivedData.sessionId;
        onQuestionsReceived();
    } else if(receivedData.hasOwnProperty('freeze')){
       freeze();
    } else if(receivedData.hasOwnProperty('login')){
        login(receivedData);
    } else if(receivedData.hasOwnProperty('room')) {
        if (gameStarted) {
            return;
        }
        rooms = receivedData;
        populateRooms();
    } else if(receivedData.hasOwnProperty('createRoom')) {
        if (gameStarted) {
            return;
        }
        roomIdRequested = receivedData.createRoom.id;
        sendEvent({getPlayersInRoom : {roomId : roomIdRequested}});
    } else if(receivedData.hasOwnProperty('kickUser')) {
        if (gameStarted) {
            return;
        }
        kickUser();
    } else if(receivedData.hasOwnProperty('register')){
        if (gameStarted) {
            return;
        }
        register(receivedData);
        console.log('Proceed with registration');
    }else if(receivedData.hasOwnProperty('endGame')){ //the jason received  must have finalPlayers object
        gameStarted = false;
        onGameOverMessage(receivedData.endGame);
    } else if (receivedData.hasOwnProperty('startGame')) {
        onStartGameBtnClick();
    } else if (receivedData.hasOwnProperty('userGameStatus')) {
        var status = receivedData.userGameStatus;
        movePlayer(status.userId, status.status);
    }

  }catch(err){
  }
}

