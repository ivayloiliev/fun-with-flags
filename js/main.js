let gameStarted = false;


$('.login-button').on('click', function(){
  $(this).hide();
  $('.login-loading').show();
    sendCredentials();
});

$('.create-room-button').on('click', function(){
    sendEvent({createRoom : {userId : userId}});
    $('#create-room-wrapper').show();
    $(this).hide();
});

$('button', '#create-room-wrapper').on('click', function() {
    "use strict";
    $('#create-room-wrapper').hide();
    $('.create-room-button').show();
});

$('.refresh-rooms-button').on('click', function () {
    sendRefreshRooms();
});

function onStartGameBtnClick() {
    "use strict";

    startGame();
    $('.start-game').attr('disabled','');
};

$('.start-game').on('click', onStartGameBtnClick);


$('.register-button').on('click', function(event){
  $('.login-page').hide();
  changeScreenTo("register-page");
});

$('.newGame-button').on('click', function(){
  expose.DeactivateConfetti();
  currentRound = 0;
  sendRefreshRooms();
  changeScreenTo('room-page');
});

$('.back-button').on('click', function(){
    sendLeaveRoom();
    sendRefreshRooms();
    changeScreenTo('room-page');
});


$('#reg-with-name-and-pass').on('click', function(){
  sendRegistrationCredentials();
});

function sendRefreshRooms() {
    var refresh = {
        getAllRooms : {
            sendType : 'sender'
        }
    }
    sendEvent(refresh);
}

function sendLeaveRoom() {
    var leaveRoom = {
        leaveRoom : {
            userId : userId
        }
    }
    sendEvent(leaveRoom);
}


function fadePlayerOut(id) {
    $('.roomtext').each(function () {
       if(parseInt($(this).attr('alt')) == id) {
           $(this).html('<span class="color-red" style="font-size: 10pt; margin-left: 20px"> left the room </span>');
           $(this).parent('.memberContainer').slideUp(1000);
       }
    });
}

function fadeRoomOut(id) {
    var objToFade;
    $('.join-listener').each(function() {
        if(parseInt($(this).attr('alt')) == id) {
             objToFade = $(this).parent('.buttoncontainer').parent('.room-lobbycontainer');
            console.log(objToFade);
            objToFade.find('.alignleft').html('<span style="color-red">room deleted</span>');
            objToFade.fadeOut(400);
        }
    });
}

function startGame() {
    getAllQuestions();
}

$('.not-a-flag').on('click', function () {
    notAFlag(this);
    $(this).css({'pointer-events':'none', 'text-decoration':'line-through'});
});

function notAFlag(flagClicked) {
    var imgClicked = $(flagClicked).parent('.flag-container').find('.img-container');
    var imgIdLength = imgClicked.attr('id').length;
    var isGuessedNAF = questions[currentRound].hasOwnProperty('nafGuessed');
    if( imgIdLength > 2 && !isGuessedNAF) {
        questions[currentRound].nafGuessed = true;
        sendFreeze();
    } else {
        freeze();
    }
}
  function sendFreeze(){
    var freezeOther = {
      freeze: {
          sessionId:window._sessionId,
          roomId:roomIdRequested,
          userId:userId
      }
    };
    sendEvent(freezeOther);
  }

    function getAllQuestions() {
    var getQuestions = {getQuestions:null, roomId: roomIdRequested};
    sendEvent(getQuestions);
    }

    function onQuestionsReceived(response){
        gameStarted = true;
        currentRound = -1;
        playerScore = 0;
        score = 0;
        setRound();
        setRoundRed();

        showOverlay('instructions', 10000);
        startingIn = 3;
        $('#time-start').html("Starting in " + startingIn.toString());
        var interval = setInterval(function() {
            startingIn -= 1;
            $('#time-start').html("Starting in " + startingIn.toString());
            if (startingIn < 0) {
                hideOverlay('instructions');
                changeScreenTo("game-room");
                changeQuestion();
                populatePlayers();
                clearInterval(interval);
            }
        }, 1000);
    }

    function populateWinners(data){
          var pl = data;
          console.log(pl);
          pl.sort(function(PlayerA, PlayerB){
            if(PlayerA.score > PlayerB.score) { return -1; }
            if(PlayerA.score < PlayerB.score) { return 1; }
            return 0;
          });
          console.log(pl);
          $('tbody', '#scores-table').empty().loadTemplate("views/table-names-scores.html", pl, {
            append:true
          });
          populateStage(pl);
    }

    function populateStage(playersArray){
      var pl = playersArray.slice(0, 3);

        $('.players').empty();

      if(pl[2] != null){
        $('.players').loadTemplate("views/stages/thirdTemplate.html", pl[2], {
          append:true
        });
      }
        if(pl[0] != null){
          $('.players').loadTemplate("views/stages/firstTemplate.html", pl[0], {
            append:true
          });
        }
          if(pl[1] != null){
            $('.players').loadTemplate("views/stages/secondTemplate.html", pl[1], {
              append:true
            });
          }
        }


function populatePlayers() {
    $('.o-game-players').empty().loadTemplate("views/user_template.html", playersInRoom.roomMembers.map(function(player) {
        "use strict";
        player.userId = 'player' + player.userId;
        return player;
    }), {
      append: true,
      success: function(){
        // movePlayer(playersInRoom.userId, playersInRoom.status)
      }
    });
}

function populateRooms() {
    console.log(rooms.room.length);
    if(rooms.room.length == 0) {
        $('.room-lobby').html('<div> no rooms available </div>');
    }
    rooms.room.sort(function(a, b) {
        a = a.playersCount;
        b = b.playersCount;
        if(a > b) return -1;
        if(a == b) return 0;
        if(a < b) return 1;
    });
    $('.room-lobby').empty().loadTemplate("views/room_template.html", rooms.room, {
        append: true,
        success: function() {
            $(".join-listener").on("click", function(){
                roomIdRequested = parseInt($(this).attr('alt'));
                console.log('Joining room ' + roomIdRequested);
                joinRoom = {
                    joinRoom : {
                        userId: userId,
                        roomId: roomIdRequested
                    }
                };
                sendEvent(joinRoom);

                var getPlayersInRoom ={
                    getPlayersInRoom : {
                    roomId : roomIdRequested
                    }
                };
                sendEvent(getPlayersInRoom);
            });
        }
    });
}

function loadAdminRights() {
    var isAdmin = _.find(playersInRoom.roomMembers, {'admin' : true});
    var admin = _.find(playersInRoom.roomMembers, {'admin' : true})
    var adminId = admin && admin.userId;
    if(isAdmin == null || parseInt(isAdmin.userId) != userId)  {
        hideKickButtons();
        hideStartButton();
    }
    else {
        showStartButton();
        showKickButtons();
        $('button[alt='+adminId+']').hide();
    }
    $('.roomtext').each(function() {
        if(adminId && parseInt($(this).attr('alt')) == adminId) {
            $(this).find('.alignleft').append(' (<span class="color-red">ADMIN</span>)');
        }
    });
}

function hideKickButtons() {
    $('.kick-button').hide();
}

function showKickButtons() {
    $('.kick-button').show();
}

function hideStartButton() {
    $('.start-game').hide();
    $('.back-button').show();
    $('.waiting-text').html('<p class="color-red"> waiting for admin to start... </p>');
}

function showStartButton() {
    $('.waiting-text').html('');
    $('.start-game').show();
    $('.back-button').hide();
}


function populateCreatedRoomMembers(){
    var adminId = _.findIndex(playersInRoom.roomMembers, {'admin' : true});
    var swap = playersInRoom.roomMembers[0];
    playersInRoom.roomMembers[0] = playersInRoom.roomMembers[adminId];
    playersInRoom.roomMembers[adminId] = swap;

    $('.roomMembers').empty().loadTemplate("views/room_members_template.html", playersInRoom.roomMembers, {
        async: false,
        append: true,
        success: function () {
            $('.kick-button').on('click', function() {
                var kickUserId = parseInt($(this).attr('alt'));
                var kickUser = {
                    kickUser: {
                        userId: kickUserId
                    }
                }
                sendEvent(kickUser);
                console.log('kick user ' + kickUserId);
            });
            loadAdminRights();
            var admin = _.find(playersInRoom.roomMembers, {'admin' : true});
            var roomName = admin ?  (admin.userName + "'s room") : 'New Room';
            console.log(playersInRoom);
            $('#room-name-header').html(roomName);
        }
    });
}

function kickUser(){
    changeScreenTo("room-page");
    sendRefreshRooms();
    showOverlay('wrong', 1000, 'kicked');
}

function movePlayer(playerId, nextStep) {
    var object_to_move = $("#player" + playerId);
    var width = $('.path').width();
    var step = width/steps;
    if(nextStep >= 0 && nextStep <= 10) {
        var nextStepPxl = nextStep * step;
        object_to_move.css({'left': nextStepPxl});
    }
}


function changeScreenTo(screen) {
  var screenName = "#" + screen;
  hideAllScreens();
  $(screenName).show();
}

function hideAllScreens() {
  $(".rectangle").hide();

}

function hideLoadingScreen() {
  console.log('HIDE');
  $(".page-loading").hide();
  $("#content").show();
}

function showLoadingScreen() {
    $("content").hide();
    changeScreenTo("loading");
}

function freeze(){
    if(isScreenFrozen) return;
    isScreenFrozen = true;
    showOverlay('freeze', freezeScreenTime );
    var counter = freezeScreenTime /1000;
    var interval = setInterval(function() {
        counter -= 0.01;
        counter = Math.round(counter * 100)/100;
        $('#time-left').html("<span>Screen freezed for</span><span> " + counter.toString() + "</span><span>seconds.</span> ");
        if (counter <= 0) {
            isScreenFrozen = false
            clearInterval(interval);
        }
    }, 10);
}

function setTimer(s) {
    var counter = s || questionTime;
    $('#time-text').html(counter.toString());
    $('.Time').css({'color' : 'black'})
    interval = setInterval(function() {
        counter--;
        $('#time-text').html(counter.toString());
        if(counter === 3) {
            $('.Time').css({'color' : 'red'});
        }
        if (counter <= 0) {
            showOverlay('wrong', answerFeedbackTime, '');
            changeQuestion();
        }
    }, 1000);

}

function addScore(time) {
    score = parseInt(perGuessedScore) + parseInt(time);
    score += time > bonusTimeSeconds ? bonusScore : 0;
    parseInt(playerScore += score);
    questions[currentRound].score = score;
}

function checkQuestion(guess) {
    var answer = questions[currentRound].answer.toLowerCase();

    if(guess === answer) {
        addScore($('#time-text').html());
        showOverlay('correct', answerFeedbackTime, '+' + score);
    } else {
        questions[currentRound].score = 0;
        showOverlay('wrong', answerFeedbackTime, 'wrong');
    }
}

function showOverlay(name, ms, msg) {
    msg = msg || $('#' + name).html();
    $('#' + name + '-content').html(msg);
    $('#' + name).show().delay(ms*2/3).fadeOut(ms*1/3);
}


function hideOverlay(name) {
    $('#' + name).hide();
}

function gameOver() {
    clearInterval(interval);
    isScreenFrozen = true;
    showOverlay('gameover', 100000, 'please wait for others');
    console.log("GAME OVER");
    var answers = {
      questionAnswers:questions,
      sessionId:window._sessionId,
      userId:window.userId,
      roomId:window.roomIdRequested
    };
    sendEvent(answers);
}

function setConfTimer() {
  setTimeout(function() {
          expose.DeactivateConfetti();
       }, 50000);
}

function getActiveScreen() {
    return $('.rectangle:visible').attr('id');
}

function sendCredentials(){
  var name = $('#login-name').val().toUpperCase();
  if(name == '') {
      loginError();
      return;
  }
  var password = $('#login-password').val();
  console.log(name);
  console.log(password);
  var login = {
      login:
      {
      "user" : name,
      "pass":password
      }
    };
    console.log(login);
  sendEvent(login);
}

function sendRegistrationCredentials(){
  var user = $('#reg-name').val().toUpperCase();
  var password = $('#reg-password').val();
  var reg = {
    register:
    {
      "user" : user,
      "pass" : password
    }
  };
  sendEvent(reg);
}

function login(loginData){
  if(loginData.login.success){
    loginSuccess(loginData);
  }else {
    loginError();
  }
}

function register(registerData){
  if(registerData.register.success){
    loginSuccess(registerData);
  }else{
    loginError();
  }
}

function loginSuccess(data){
    changeScreenTo("room-page");
    userId = (data.login || data.register).userId;
    console.log('userId: ' + userId);
    sendRefreshRooms();
}

function loginError(){
  console.log("Wrong credentials!");
  $('.login-loading').hide();
  $('.login-button').show();
  $('#login-password').val('');
  $('#login-message').slideDown(500);
}

$('#login-name').on('keypress', function(event) {
  if (event.keyCode == 13 || event.which == 13) {
    $('#login-password').focus();
  }
});

$('#login-password').on('keypress', function(event){
  if(event.keyCode == 13 || event.which == 13) {
    if(($('#login-password').val() != null)){
      sendCredentials();
    }
  }
});

$('#reg-name').on('keypress', function(event) {
  if (event.keyCode == 13 || event.which == 13) {
    $('#reg-password').focus();
  }
});

$('#reg-password').on('keypress', function(event){
  if(event.keyCode == 13 || event.which == 13) {
      if ($('#reg-password').val()){
        $('#reg-with-name-and-pass').trigger('click');
      }
  }
});

function onRoomCreated(data) {
    "use strict";
    playersInRoom = data;
    changeScreenTo("start-game-page");
    $('.start-game').removeAttr('disabled');

    populateCreatedRoomMembers();

    $('button', '#create-room-wrapper').trigger('click');
}

function onGameOverMessage(finalPlayers){
    expose2.RestartConfetti();
  hideOverlay('gameover'); //TODO: on event when everyone finished
    isScreenFrozen = false;
  changeScreenTo("ladder-room");
  $('canvas').show();
  setConfTimer();
  populateWinners(finalPlayers);
}
$('.o-screen-credits__close').on('click', function() {
    "use strict";
    $('.o-screen-credits').hide();
});

$('.o-credit-btn').on('click', function() {
    "use strict";
    $('.o-screen-credits').show();
});