package com.intracol.dao;

import com.intracol.entities.User;
import com.intracol.entities.UserRoom;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Ventsy on 06/07/2017.
 */
public interface UserRoomDaoInterface {

    void insert(UserRoom entity);

    void update(UserRoom entity);

    void delete(UserRoom entity);

    List<User> getAllUsersInRoom(long roomId);

    long getPlayerCountForRoom(long roomId);

    UserRoom findUserRoomByUserId(long userId);

    void deletUserByUserId(UserRoom entity);
}
