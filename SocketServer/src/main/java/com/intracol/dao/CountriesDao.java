package com.intracol.dao;
import org.hibernate.Session;
import org.hibernate.Transaction;
import static com.intracol.dao.SessionFactoryClass.getSessionFactoryInstance;

import java.sql.ResultSet;
import java.sql.Statement;
import java.util.*;

import com.intracol.entities.Countries;
import org.hibernate.query.Query;

import javax.persistence.TypedQuery;

public class CountriesDao implements CountriesDaoInterface{
    private Session currentSession;
    private Transaction currentTransaction;

    public CountriesDao(){
    }

    public Session openCurrentSession(){
        currentSession=getSessionFactoryInstance().getSessionFactory().openSession();
        return currentSession;
    }

    public Session openCurrentSessionWithTransaction(){
        currentSession=getSessionFactoryInstance().getSessionFactory().openSession();
        currentTransaction=currentSession.beginTransaction();
        return currentSession;
    }

    public void closeCurrentSession(){
        currentSession.close();
    }

    public void closeCurrentSessionWithTransaction(){
        currentTransaction.commit();
        currentSession.close();
    }

    public Session getCurrentSession() {
        return currentSession;
    }

    public void setCurrentSession(Session currentSession) {
        this.currentSession = currentSession;
    }

    public Transaction getCurrentTransaction() {
        return currentTransaction;
    }

    public void setCurrentTransaction(Transaction currentTransaction) {
        this.currentTransaction = currentTransaction;
    }

    public void insert(Countries country){
        getCurrentSession().save(country);
    }

    public void update(Countries country){
        getCurrentSession().update(country);
    }

    public void delete(long id){
        getCurrentSession().delete(id);
    }

    public Countries findById(long id){
        Countries country=(Countries) getCurrentSession().get(Countries.class,id );
        return country;
    }

    @SuppressWarnings("unchecked")
    public List<Countries> getAll(){
        List<Countries> Country = new ArrayList<Countries>();
        Session session = getCurrentSession();
        for (Object oneObject : session.createQuery("FROM Countries where has_flag=1 ").getResultList()) {
            Country.add((Countries)oneObject);
        }

       /* TypedQuery<Countries> query=currentSession.createQuery("from com.intracol.entities.Countries as C where C.id=1",Countries.class);
        List<Countries> countries = (List<Countries>) getCurrentSession().createQuery("from Countries").list();*/
        return Country;
    }

    public List<Countries> getRandomQuestions() {
        int k=3;
        int temp=0;
        int temp1=0;
        List<Countries> hardQuestions=(List<Countries>) getCurrentSession()
                .createQuery("from Countries where has_flag=1 and priority>=71 and priority<=100 order by rand() ").setMaxResults(4).list();
        temp=temp+hardQuestions.size();
        while(temp<4){
            temp++;
            temp1++;
        }
        List<Countries> mediumQuestions=(List<Countries>) getCurrentSession()
                .createQuery("from Countries where has_flag=1 and priority>=31 and priority<=70.99 order by rand()").setMaxResults(k+temp1).list();
        temp=0;
        temp1=0;
        temp=temp+mediumQuestions.size()+hardQuestions.size();
        while(temp<7){
            temp++;
            temp1++;
        }
        List<Countries> easyQuestions=(List<Countries>) getCurrentSession()
                .createQuery("from Countries where has_flag=1 and priority>=0 and priority<=30.99  order by rand()").setMaxResults(k+temp1).list();
        List<Countries> countries=new ArrayList<>();
        countries.addAll(easyQuestions);
        countries.addAll(mediumQuestions);
        countries.addAll(hardQuestions);
        int sum=easyQuestions.size()+mediumQuestions.size()+hardQuestions.size();

        while(sum<10){
            List<Countries> additionalCountries= (List<Countries>) getCurrentSession()
                    .createQuery("from Countries where has_flag=1 order by rand()").setMaxResults(1).list();
            if(!countries.contains(additionalCountries.get(0).getCountryName())){
                countries.addAll(additionalCountries);
                sum++;
            }
            additionalCountries.clear();
        }

        Collections.shuffle(countries);
        return countries;
    }

    @Override
    public void updatePriorityByName(String name, int value){
        if(name!=null && (value>0 && value<=100)) {
            int id = getIdbyCountryName(name);
            Query query = getCurrentSession().createQuery("Update Countries set priority=" + value + "  where id=" +id);
            query.executeUpdate();
        }
    }
    @Override
    public int getPriorityByName(String name){
        return (int)getCurrentSession().createQuery("select priority from Countries where countryName="+"'"+name+"'").uniqueResult();
    }
    public int getIdbyCountryName(String name){
        return (int)getCurrentSession().createQuery("select id from Countries where countryName="+"'"+name+"'").uniqueResult();
    }
}
