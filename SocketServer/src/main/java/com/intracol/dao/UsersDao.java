package com.intracol.dao;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.intracol.entities.User;
import org.hibernate.query.Query;
import static com.intracol.dao.SessionFactoryClass.getSessionFactoryInstance;

public class UsersDao implements UsersDaoInterface {

    private Session currentSession;
    private Transaction currentTransaction;

    public UsersDao(){
    }

    public Session openCurrentSession(){
            currentSession= getSessionFactoryInstance().getSessionFactory().openSession();
            return currentSession;
    }

    public Session openCurrentSessionWithTransaction(){
        currentSession= getSessionFactoryInstance().getSessionFactory().openSession();
        currentTransaction=currentSession.beginTransaction();
        return currentSession;
    }

    public void closeCurrentSession(){
        currentSession.close();
    }
    public void closeCurrentSessionWithTransaction(){
        currentTransaction.commit();
        currentSession.close();
    }

    private Session getCurrentSession(){
        return currentSession;
    }
    public void setCurrentSession(Session currentSession){
        this.currentSession=currentSession;
    }

    public Transaction getCurrentTransaction(){
        return currentTransaction;
    }
    public void setCurrentTransaction(Transaction currentTransaction){
        this.currentTransaction=currentTransaction;
    }

    @Override
    public void insert(User entity){
        getCurrentSession().save(entity);
    }

    @Override
    public void update(User entity){
        getCurrentSession().update(entity);
    }

    @Override
    public void delete(long id) {
        getCurrentSession().delete(id);

    }

    @Override
    public User findById(long id) {
        return getCurrentSession().get(User.class,id);
    }

    @Override
    public User findByUsername(String name) {
        Query query= getCurrentSession().
                createQuery("from User where name=:name");
        query.setParameter("name", name);

        return (User) query.uniqueResult();
    }
}
