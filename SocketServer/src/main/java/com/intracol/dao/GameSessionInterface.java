package com.intracol.dao;

import com.intracol.entities.GameSession;

/**
 * Created by Stefan on 12.7.2017 г..
 */
public interface GameSessionInterface {
    void insert(GameSession entity);
    void update(GameSession entity);
    void delete(GameSession entity);
    GameSession findById(long id);
    long getSessionIdByUser0ID(long user0Id);
    long getSessionIdByUserId(long userId);
}
