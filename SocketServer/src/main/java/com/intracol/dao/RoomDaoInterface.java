package com.intracol.dao;

import com.intracol.entities.Room;

import java.util.List;

/**
 * Created by Stefan on 6.7.2017 г..
 */
public interface RoomDaoInterface {
    void insert(Room room);
    void update(Room room);
    void delete(Room entity);
    Room findById(long id);
    Room findByRoomName(String name);
    List<Room> getAll();
}
