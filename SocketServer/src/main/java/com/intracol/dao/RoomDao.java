package com.intracol.dao;

import com.intracol.entities.Room;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

import java.util.List;

import static com.intracol.dao.SessionFactoryClass.getSessionFactoryInstance;


/**
 * Created by Stefan on 6.7.2017 г..
 */
public class RoomDao implements RoomDaoInterface {

    private Session currentSession;
    private Transaction currentTransaction;

    public RoomDao(){}

    public Session openCurrentSession() {
        currentSession = getSessionFactoryInstance().getSessionFactory().openSession();
        return  currentSession;
    }
    public Session openCurrentSessionWithTransaction(){
        currentSession = getSessionFactoryInstance().getSessionFactory().openSession();
        currentTransaction = currentSession.beginTransaction();
        return currentSession;
    }
    public void closeCurrentSession(){
        currentSession.close();
    }
    public void closeCurrentSessionWithTransaction(){
        currentTransaction.commit();
        currentSession.close();
    }

    public Session getCurrentSession(){
        return currentSession;
    }

    public void setCurrentSession(Session currentSession){
        this.currentSession = currentSession;
    }

    public Transaction getCurrentTransaction(){
        return currentTransaction;
    }

    public void setCurrentTransaction(Transaction currentTransaction){
        this.currentTransaction = currentTransaction;
    }

    @Override
    public void insert(Room entity){
        getCurrentSession().save(entity);
    }

    @Override
    public void update(Room entity){
        getCurrentSession().update(entity);
    }

    @Override
    public void delete (Room entity){
        getCurrentSession().remove(entity);
    }

    @Override
    public Room findById(long id){
        return getCurrentSession().get(Room.class,id);
    }

    @Override
    public Room findByRoomName(String name){
        return (Room)getCurrentSession().createQuery("from Room where name="+"'"+name+"'").uniqueResult();
    }

    @Override
    public List<Room> getAll(){
        return getCurrentSession().createQuery("from Room").list();
    }
}
