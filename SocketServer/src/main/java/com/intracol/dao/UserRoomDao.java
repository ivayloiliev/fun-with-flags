package com.intracol.dao;

import com.intracol.entities.User;
import com.intracol.entities.UserRoom;
import com.intracol.service.UserService;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.ArrayList;
import java.util.List;

import static com.intracol.dao.SessionFactoryClass.getSessionFactoryInstance;

/**
 * Created by Ventsy on 06/07/2017.
 */
public class UserRoomDao implements UserRoomDaoInterface {

    private Session currentSession;
    private Transaction currentTransaction;

    public UserRoomDao(){}

    public Session openCurrentSession(){
        currentSession = getSessionFactoryInstance().getSessionFactory().openSession();
        return currentSession;
    }

    public Session openCurrentSessionWithTransaction() {
        currentSession = getSessionFactoryInstance().getSessionFactory().openSession();
        currentTransaction= currentSession.beginTransaction();
        return currentSession;
    }

    public void closeCurrentSession(){
        currentSession.close();
    }

    public void closeCurrentSessionWithTransaction() {
        currentTransaction.commit();
        currentSession.close();
    }

    public Session getCurrentSession() {
        return currentSession;
    }

    public void setCurrentSession(Session currentSession) {
        this.currentSession = currentSession;
    }

    public Transaction getCurrentTransaction() {
        return currentTransaction;
    }

    public void setCurrentTransaction(Transaction currentTransaction) {
        this.currentTransaction = currentTransaction;
    }

    @Override
    public void insert(UserRoom entity) {
        getCurrentSession().save(entity);
    }

    @Override
    public void update(UserRoom entity) {
        getCurrentSession().update(entity);
    }

    @Override
    public void delete(UserRoom entity) {
        getCurrentSession().delete(entity);
    }

    @Override
    public UserRoom findUserRoomByUserId(long userId) {
        return (UserRoom)getCurrentSession().createQuery("from UserRoom where userId= "+userId).uniqueResult();
    }

    @Override
    public List<User> getAllUsersInRoom(long roomId){
        UserService userServ= new UserService();
        List<Long> userIds = getCurrentSession().createQuery("Select userId from UserRoom where roomId="+roomId).list();
        List<User> list= new ArrayList<>();
        for(Long userId: userIds){
            list.add(userServ.findById(userId));
        }
        return list;
    }

    @Override
    public long getPlayerCountForRoom(long roomId){
        long userIdCount = (Long)getCurrentSession().createQuery("Select count(*) from UserRoom where roomId="+roomId).uniqueResult();
        return userIdCount;
    }

    public List<UserRoom> getAllUserRooms(){
        return getCurrentSession().createQuery("from UserRoom").list();
    }

    public void deletUserByUserId(UserRoom entity){
        getCurrentSession().delete(entity);
    }

}
