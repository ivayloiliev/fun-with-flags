package com.intracol.dao;

import com.intracol.entities.GameSession;
import org.hibernate.Session;
import org.hibernate.Transaction;

import static com.intracol.dao.SessionFactoryClass.getSessionFactoryInstance;

/**
 * Created by Stefan on 12.7.2017 г..
 */
public class GameSessionDao implements GameSessionInterface {

    private Session currentSession;
    private Transaction currentTransaction;

    public Session openCurrentSession() {
        currentSession = getSessionFactoryInstance().getSessionFactory().openSession();
        return currentSession;
    }

    public Session openCurrentSessionWithTransaction() {
        currentSession = getSessionFactoryInstance().getSessionFactory().openSession();
        currentTransaction = currentSession.beginTransaction();
        return currentSession;
    }

    public void closeCurrentSession() {
        currentSession.close();
    }

    public void closeCurrentSessionWithTransaction() {
        currentTransaction.commit();
        currentSession.close();
    }

    public Session getCurrentSession() {
        return currentSession;
    }

    public void setCurrentSession(Session currentSession) {
        this.currentSession = currentSession;
    }

    public Transaction getCurrentTransaction() {
        return currentTransaction;
    }

    public void setCurrentTransaction(Transaction currentTransaction) {
        this.currentTransaction = currentTransaction;
    }

    @Override
    public void insert(GameSession entity) {
        getCurrentSession().save(entity);
    }

    @Override
    public void update(GameSession entity) {
        getCurrentSession().update(entity);
    }

    @Override
    public void delete(GameSession entity) {
        getCurrentSession().remove(entity);
    }

    @Override
    public GameSession findById(long id) {
        return (GameSession) getCurrentSession().createQuery("from GameSession where id="+id).uniqueResult();
    }

    @Override
    public long getSessionIdByUser0ID(long user0Id) {
        return (long) getCurrentSession().createQuery("Select id from GameSession where user0Id=" + user0Id).uniqueResult();
    }

    public long getSessionIdByUserId(long userId){
        return (long) getCurrentSession().createQuery("select id from GameSession where user0Id="+userId+"or user1Id="+userId
        +"or user2Id="+userId+"or user3Id="+userId+"or user4Id="+userId+"or user5Id="+userId+"or user6Id="+userId
        +"or user7Id="+userId).uniqueResult();
    }
}
