package com.intracol.dao;

import com.intracol.entities.*;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

/**
 * Created by victor on 07.07.17.
 */
public class SessionFactoryClass {

    private static SessionFactoryClass sessionFactoryClass = new SessionFactoryClass();

    private final SessionFactory sessionFactory = new Configuration()
                .addAnnotatedClass(User.class)
                .addAnnotatedClass(Room.class)
                .addAnnotatedClass(Countries.class)
                .addAnnotatedClass(UserRoom.class)
                .addAnnotatedClass(GameSession.class)
                .configure().buildSessionFactory();

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public static SessionFactoryClass getSessionFactoryInstance(){
        return sessionFactoryClass;
    }
}
