package com.intracol.dao;

import com.intracol.entities.User;

public interface UsersDaoInterface {

    void insert(User user);
    void update(User user);
    void delete(long id);
    User findById(long id);
    User findByUsername(String name);
}
