package com.intracol.dao;
import com.intracol.entities.Countries;

import java.util.*;
import java.io.Serializable;

public interface CountriesDaoInterface extends Serializable {
    void insert(Countries country);
    void update(Countries country);
    void delete(long id);
    Countries findById(long id);
    List<Countries> getAll();
    List<Countries> getRandomQuestions();
    void updatePriorityByName(String name, int value);
    int getPriorityByName(String name);
}
