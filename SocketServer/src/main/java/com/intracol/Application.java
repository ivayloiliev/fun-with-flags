/*
 * Copyright Intracol Technologies AD 2016
 */
package com.intracol;


import com.intracol.Server.FlagSessionServer;
import com.intracol.entities.Room;
import com.intracol.events.EventHandler;
import com.intracol.service.RoomService;
import org.java_websocket.WebSocketImpl;
import org.json.JSONObject;

import java.awt.*;
import java.io.*;
import java.security.*;
import java.security.cert.CertificateException;
import java.util.HashMap;


/**
 * @author <a href="mailto:imishev@intracol.com">Ivan Mishev</a>
 */
public class Application {


    public static void main(String args[]) throws IOException, InterruptedException, KeyManagementException, NoSuchAlgorithmException, UnrecoverableKeyException, KeyStoreException, CertificateException {

        WebSocketImpl.DEBUG = true;
        int port = 9999; // 843 flash policy port
        try {
            port = Integer.parseInt( args[ 0 ] );
        } catch ( Exception ex ) {
        }

        FlagSessionServer server = new FlagSessionServer( port );

        server.start();
        System.out.println("ChatServer started on port: " + server.getPort());

        BufferedReader sysin = new BufferedReader(new InputStreamReader(System.in));
        while (true) {
            String in = sysin.readLine();
            server.sendToAll(in);
            if (in.equals("exit")) {
                server.stop();
                break;
            }
        }

        /**
         * For Secure socket server
         * */

/*        // load up the key store
        String STORETYPE = "JKS";
        String KEYSTORE = "keystore.jks";
        String STOREPASSWORD = "storepassword";
        String KEYPASSWORD = "keypassword";

        KeyStore ks = KeyStore.getInstance(STORETYPE);
        File kf = new File(KEYSTORE);
        ks.load(new FileInputStream(kf), STOREPASSWORD.toCharArray());

        KeyManagerFactory kmf = KeyManagerFactory.getInstance("SunX509");
        kmf.init(ks, KEYPASSWORD.toCharArray());
        TrustManagerFactory tmf = TrustManagerFactory.getInstance("SunX509");
        tmf.init(ks);

        SSLContext sslContext = SSLContext.getInstance("TLS");
        sslContext.init(kmf.getKeyManagers(), tmf.getTrustManagers(), null);

        //Lets remove some ciphers and protocols
        SSLEngine engine = sslContext.createSSLEngine();
        List<String> ciphers = new ArrayList<String>( Arrays.asList(engine.getEnabledCipherSuites()));
        ciphers.remove("TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256");
        List<String> protocols = new ArrayList<String>( Arrays.asList(engine.getEnabledProtocols()));
        protocols.remove("SSLv3");
        CustomSSLWebSocketServerFactory factory = new CustomSSLWebSocketServerFactory(sslContext, protocols.toArray(new String[]{}), ciphers.toArray(new String[]{}));

        // Different example just using specific ciphers and protocols
        *//*
        String[] enabledProtocols = {"TLSv1.2"};
		String[] enabledCipherSuites = {"TLS_RSA_WITH_AES_128_CBC_SHA", "TLS_RSA_WITH_AES_256_CBC_SHA"};
        CustomSSLWebSocketServerFactory factory = new CustomSSLWebSocketServerFactory(sslContext, enabledProtocols,enabledCipherSuites);
        *//*
        server.setWebSocketFactory(factory);

        server.start();*/


    }

}
