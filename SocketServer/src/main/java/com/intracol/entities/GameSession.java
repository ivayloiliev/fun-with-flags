package com.intracol.entities;

import javax.persistence.*;

/**
 * Created by Stefan on 12.7.2017 г..
 */

@Entity
@Table(name = "gamesession")
public class GameSession {
    @Id
    @Column(name = "id")
    private long id;

    @Column(name = "user_0_id")
    private long user0Id;

    @Column(name = "user_1_id")
    private long user1Id;

    @Column(name = "user_2_id")
    private long user2Id;

    @Column(name = "user_3_id")
    private long user3Id;

    @Column(name = "user_4_id")
    private long user4Id;

    @Column(name = "user_5_id")
    private long user5Id;

    @Column(name = "user_6_id")
    private long user6Id;

    @Column(name = "user_7_id")
    private long user7Id;

    @Column(name = "score_0")
    private int score0;

    @Column(name = "score_1")
    private int score1;

    @Column(name = "score_2")
    private int score2;

    @Column(name = "score_3")
    private int score3;

    @Column(name = "score_4")
    private int score4;

    @Column(name = "score_5")
    private int score5;

    @Column(name = "score_6")
    private int score6;

    @Column(name = "score_7")
    private int score7;

    @Column(name = "countUsers")
    private int countUsers;

    public int getCountUsers() {
        return countUsers;
    }

    public void setCountUsers(int countUsers) {
        this.countUsers = countUsers;
    }

    public GameSession(){}

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getUser0Id() {
        return user0Id;
    }

    public void setUser0Id(long user0Id) {
        this.user0Id = user0Id;
    }

    public long getUser1Id() {
        return user1Id;
    }

    public void setUser1Id(long user1Id) {
        this.user1Id = user1Id;
    }

    public long getUser2Id() {
        return user2Id;
    }

    public void setUser2Id(long user2Id) {
        this.user2Id = user2Id;
    }

    public long getUser3Id() {
        return user3Id;
    }

    public void setUser3Id(long user3Id) {
        this.user3Id = user3Id;
    }

    public long getUser4Id() {
        return user4Id;
    }

    public void setUser4Id(long user4Id) {
        this.user4Id = user4Id;
    }

    public long getUser5Id() {
        return user5Id;
    }

    public void setUser5Id(long user5Id) {
        this.user5Id = user5Id;
    }

    public long getUser6Id() {
        return user6Id;
    }

    public void setUser6Id(long user6Id) {
        this.user6Id = user6Id;
    }

    public long getUser7Id() {
        return user7Id;
    }

    public void setUser7Id(long user7Id) {
        this.user7Id = user7Id;
    }

    public int getScore0() {
        return score0;
    }

    public void setScore0(int score0) {
        this.score0 = score0;
    }

    public int getScore1() {
        return score1;
    }

    public void setScore1(int score1) {
        this.score1 = score1;
    }

    public int getScore2() {
        return score2;
    }

    public void setScore2(int score2) {
        this.score2 = score2;
    }

    public int getScore3() {
        return score3;
    }

    public void setScore3(int score3) {
        this.score3 = score3;
    }

    public int getScore4() {
        return score4;
    }

    public void setScore4(int score4) {
        this.score4 = score4;
    }

    public int getScore5() {
        return score5;
    }

    public void setScore5(int score5) {
        this.score5 = score5;
    }

    public int getScore6() {
        return score6;
    }

    public void setScore6(int score6) {
        this.score6 = score6;
    }

    public int getScore7() {
        return score7;
    }

    public void setScore7(int score7) {
        this.score7 = score7;
    }
}
