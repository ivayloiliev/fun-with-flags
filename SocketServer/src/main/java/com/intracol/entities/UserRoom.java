package com.intracol.entities;

import javax.persistence.*;

/**
 * Created by Ventsy on 06/07/2017.
 */
@Entity
@Table(name = "user_room")
public class UserRoom {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;

    @Column(name = "user_id")
    private long userId;

    @Column(name = "room_id")
    private long roomId;

    public UserRoom() {
    }

    public UserRoom(long id, long userID, long roomID) {
        this.id = id;
        this.userId = userID;
        this.roomId = roomID;
    }

    public UserRoom(long userID, long roomID) {
        this.userId = userID;
        this.roomId = roomID;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public long getRoomId() {
        return roomId;
    }

    public void setRoomId(long roomId) {
        this.roomId = roomId;
    }

    @Override
    public String toString() {
        return "User id: " + userId + " Room id: " + roomId;
    }
}
