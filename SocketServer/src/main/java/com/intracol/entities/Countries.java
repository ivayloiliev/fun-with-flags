package com.intracol.entities;

import javax.persistence.*;

@Entity
@Table(name="countries")
public class Countries {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name="countryCode")
    private String countryCode;

    @Column(name="countryName")
    private String countryName;

    @Column(name="currencyCode")
    private String currencyCode;

    @Column(name="priority")
    private int priority;

    @Column(name="has_flag")
    private int has_flag;

    public Countries(){
    }

    public Countries(int id, String countryCode, String countryName, String currencyCode, int priority, int has_flag){
        this.id=id;
        this.countryCode=countryCode;
        this.countryName=countryName;
        this.currencyCode=currencyCode;
        this.priority=priority;
        this.has_flag=has_flag;
    }

    public int getId() {
        return id;
    }
    public void setId(int id){
        this.id=id;
    }

    public String getCountryCode() {
        return countryCode;
    }
    public void setCountryCode(String countryCode){
        this.countryCode=countryCode;
    }

    public String getCountryName() {
        return countryName;
    }
    public void setCountryName(String countryName){
        this.countryName=countryName;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }
    public void setCurrencyCode(String currencyCode){
        this.currencyCode=currencyCode;
    }

    public int getPriority() {
        return priority;
    }
    public void setPriority(int priority){
        this.priority=priority;
    }

    public int getHas_flag() {
        return has_flag;
    }
    public void setHas_flag(int has_flag){
        this.has_flag=has_flag;
    }
}
