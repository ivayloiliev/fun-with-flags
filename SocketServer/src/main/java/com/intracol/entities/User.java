package com.intracol.entities;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by victor on 04.07.17.
 */

@Entity
@Table(name = "users")
public class User {

    public User(String username, String password, Date createdDate) {
        this.username = username;
        this.password = password;
        this.createdDate = createdDate;
    }

    public User() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long userId;

    @Column(name = "name")
    private String username;

    @Column(name = "overall_score")
    private int userPoints;

    @Column(name = "password")
    private String password;

    @Column(name = "created_on")
    private Date createdDate;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setUserPoints(int userPoints) {
        this.userPoints = userPoints;
    }

    public int getUserPoints() {
        return userPoints;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

}