package com.intracol.entities;

import javax.persistence.*;


/**
 * Created by Stefan on 6.7.2017 г..
 */
@Entity
@Table (name = "rooms")
public class Room {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column (name = "id")
    private long id;

    @Column (name = "room_name")
    private String name;

    @Column (name = "admin_id")
    private long adminId;

    public Room(){}

    public Room(String name, long adminId) {
        this.name = name;
        this.adminId = adminId;
    }

    public long getId() { return id; }

    public void setId(long id) {this.id = id;}

    public String getName() {return name;}

    public void setName(String name) {this.name = name;}

    public long getAdminId() {return adminId;}

    public void setAdminId(long adminId) {this.adminId = adminId;}
}
