package com.intracol.helper;

import org.java_websocket.WebSocket;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by victor on 11.07.17.
 */
public class UserSockets {
    private Map<Long, WebSocket> userSockets = new HashMap<Long, WebSocket>();

    private static UserSockets userSocketMap = new UserSockets();

    public static UserSockets getUserSocketMapInstance() {
        return userSocketMap;
    }

    public void add(Long userId, WebSocket socket) {
        userSockets.put(userId, socket);
    }

    public void remove(WebSocket socket) {
        userSockets.values().remove(socket);
    }

    public WebSocket getSocket(Long userId) {
        return userSockets.get(userId);
    }

    public Long getUserId(WebSocket socket) {
        for (Map.Entry<Long, WebSocket> e : userSockets.entrySet()) {
            if (socket.equals(e.getValue()))
                return e.getKey();
        }
        return null;
    }
}
