package com.intracol.helper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


/**
 * Created by Ventsy on 04/07/2017.
 */
public class FlagsMapper {

    public static Map<String, JSONObject> getJsonAsMap(String json) {
        try {
            JSONObject object = new JSONObject(json);
            Map<String, JSONObject> map = new HashMap<String, JSONObject>();
            if (object != null) {
                for (String key : object.keySet()) {
                    if (json.indexOf("[") >= 0) {
                        JSONArray array = object.getJSONArray(key);
                        map.put(key, new JSONObject().put(key, array));
                        json = json.substring(json.indexOf("[") + 1);
                    } else {
                        if (object.get(key) != null) {
                            try {
                                map.put(key, object.getJSONObject(key));
                            } catch (JSONException e) {
                                map.put(key, object);
                            }
                        }
                    }
                }
            }
            return map;
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Couldn't parse json:" + json, e);
        }
    }

    public static String getMapAsJson(JSONObject object) {
        return object.toString();
    }
}