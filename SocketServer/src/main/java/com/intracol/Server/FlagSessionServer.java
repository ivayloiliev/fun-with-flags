/*
 * Copyright Intracol Technologies AD 2016
 */
package com.intracol.Server;

import com.intracol.entities.User;
import com.intracol.entities.GameSession;
import com.intracol.events.EventHandler;
import com.intracol.helper.FlagsMapper;
import com.intracol.helper.UserSockets;
import com.intracol.service.RoomService;
import com.intracol.service.UserRoomService;
import com.intracol.service.GameSessionService;
import org.java_websocket.WebSocket;
import org.java_websocket.framing.Framedata;
import org.java_websocket.handshake.ClientHandshake;
import org.java_websocket.server.WebSocketServer;
import org.json.JSONObject;
import com.intracol.entities.UserRoom;
import com.intracol.entities.Room;

import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.util.*;

/**
 * @author <a href="mailto:imishev@intracol.com">Ivan Mishev</a>
 */
public class FlagSessionServer extends WebSocketServer {

    private EventHandler handler = new EventHandler();

    public FlagSessionServer (int port) throws UnknownHostException{
        super(new InetSocketAddress(port));
    }

    public FlagSessionServer( InetSocketAddress address ) {
        super( address );
    }

    public void onOpen(WebSocket conn, ClientHandshake clientHandshake) {
        conn.send( "new connection: " + clientHandshake.getResourceDescriptor() );
        System.out.println( conn.getRemoteSocketAddress().getAddress().getHostAddress() + " established connection to server" );
    }

    public void onClose(WebSocket webSocket, int i, String s, boolean b) {
        RoomService roomService=new RoomService();
        UserRoomService userRoomService=new UserRoomService();

        long userId=UserSockets.getUserSocketMapInstance().getUserId(webSocket);

        if(userId!=0){
            UserRoom userRoom=userRoomService.findUserRoomByUserId(userId);

            if(userRoom!=null){
                if(userRoomService.getPlayerCountForRoom(userRoom.getRoomId())==1){
                    userRoomService.delete(userRoom);
                    roomService.delete(userRoom.getRoomId());
                }else{
                    userRoomService.delete(userRoom);
                    Room room=roomService.findById(userRoom.getRoomId());
                    if(room.getAdminId()==userId){
                        List<User> list=userRoomService.getAllUsersByRoomID(userRoom.getRoomId());
                        Random rand=new Random();
                        int index=rand.nextInt(list.size());
                        room.setAdminId(list.get(index).getUserId());
                        roomService.update(room);
                    }
                }
            }
            GameSessionService gameSessionService=new GameSessionService();
            long sessionId=gameSessionService.getSessionIdByUserId(userId);
            GameSession gameSession=gameSessionService.findGameSessionById(sessionId);
            int count=gameSession.getCountUsers();
            gameSession.setCountUsers(gameSession.getCountUsers()-1);
            gameSessionService.update(gameSession);

        }

        UserSockets.getUserSocketMapInstance().remove(webSocket);
    }

    public void onMessage(WebSocket conn, String message) {
        Map<String, JSONObject> map = FlagsMapper.getJsonAsMap(message);
        JSONObject eventResponse = handler.executeEvent(conn, map);
        chooseSend(conn, eventResponse);
    }

    private void chooseSend(WebSocket senderSocket, JSONObject eventResponse) {
        JSONObject internalInfo = (JSONObject)eventResponse.remove("internalInfo");

        switch (internalInfo.getString("sendType")) {
            case "room":
                sendToRoom(senderSocket, internalInfo.getLong("roomId"), eventResponse.toString());
            break;

            case "sender":
                senderSocket.send(eventResponse.toString());
            break;

            case "all":
                sendToAll(eventResponse.toString());
            break;

            case "roomAndSender":
                sendToRoom(senderSocket, internalInfo.getLong("roomId"), eventResponse.toString());
                senderSocket.send(eventResponse.toString());
            break;

            case "roomAndKick":
                WebSocket kickedUserSocket = UserSockets.getUserSocketMapInstance().getSocket(internalInfo.getLong("kickUserId"));
                sendToRoom(kickedUserSocket, internalInfo.getLong("roomId"), eventResponse.toString());
                kickedUserSocket.send(new JSONObject().put("kickUser", true).toString());
            break;

            case "NUL":
            break;
        }
    }

    public static void hardFixPlayerKick(Long userId) {
        WebSocket kickUser = UserSockets.getUserSocketMapInstance().getSocket(userId);
        kickUser.send(new JSONObject().put("kickUser", true).toString());
    }

    public void onError(WebSocket webSocket, Exception e) {
        e.printStackTrace();
        if( webSocket != null ) {
            // some errors like port binding failed may not be assignable to a specific websocket
        }
    }

    @Override
    public void onFragment( WebSocket conn, Framedata fragment ) {
//        System.out.println( "received fragment: " + fragment );
    }


    public void onStart() {
        System.out.println("Server started!");
    }

    public void sendToAll( String text ) {
        Collection<WebSocket> con = connections();
        synchronized ( con ) {
            for( WebSocket c : con ) {
                c.send( text );
            }
        }
    }

    public void sendToRoom(WebSocket sender, long roomId, String text ) {
        UserRoomService userRoomService = new UserRoomService();
        List<User> usersInRoom = userRoomService.getAllUsersByRoomID(roomId);
        for (User user : usersInRoom) {
            if (sender == UserSockets.getUserSocketMapInstance().getSocket(user.getUserId()))
                continue;
            WebSocket userSocket = UserSockets.getUserSocketMapInstance().getSocket(user.getUserId());
            if(userSocket != null){
                userSocket.send(text);
            }
        }
    }

}
