package com.intracol.service;

import com.intracol.dao.GameSessionDao;
import com.intracol.entities.GameSession;

/**
 * Created by Stefan on 12.7.2017 г..
 */
public class GameSessionService {
    private static GameSessionDao gameSessionDao;

    public GameSessionService(){
        gameSessionDao = new GameSessionDao();
    }

    public void insert(GameSession entity){
        gameSessionDao.openCurrentSessionWithTransaction();
        gameSessionDao.insert(entity);
        gameSessionDao.closeCurrentSessionWithTransaction();
    }

    public void update(GameSession entity){
        gameSessionDao.openCurrentSessionWithTransaction();
        gameSessionDao.update(entity);
        gameSessionDao.closeCurrentSessionWithTransaction();
    }

    public void delete(GameSession entity){
        gameSessionDao.openCurrentSessionWithTransaction();
        gameSessionDao.delete(entity);
        gameSessionDao.closeCurrentSessionWithTransaction();
    }
    public GameSession findGameSessionById(long id){
        gameSessionDao.openCurrentSession();
        GameSession gameSession = gameSessionDao.findById(id);
        gameSessionDao.closeCurrentSession();
        return gameSession;
    }

    public long getSessionIdByUser0ID(long user0Id){
        gameSessionDao.openCurrentSession();
        long id= gameSessionDao.getSessionIdByUser0ID(user0Id);
        gameSessionDao.closeCurrentSession();
        return id;
    }

    public long getSessionIdByUserId(long userId){
        gameSessionDao.openCurrentSession();
        long id=gameSessionDao.getSessionIdByUserId(userId);
        gameSessionDao.closeCurrentSession();
        return id;
    }
}
