package com.intracol.service;
import com.intracol.dao.CountriesDao;
import com.intracol.entities.Countries;
import java.util.*;
public class CountriesService {
    private static CountriesDao countriesDao;

    public CountriesService(){
        countriesDao=new CountriesDao();
    }

    public void insert(Countries country){
        countriesDao.openCurrentSessionWithTransaction();
        countriesDao.insert(country);
        countriesDao.closeCurrentSessionWithTransaction();
    }

    public void update(Countries country){
        countriesDao.openCurrentSessionWithTransaction();
        countriesDao.update(country);
        countriesDao.closeCurrentSessionWithTransaction();
    }

    public void delete(long id){
        countriesDao.openCurrentSessionWithTransaction();
        countriesDao.delete(id);
        countriesDao.closeCurrentSessionWithTransaction();
    }

    public Countries findById(long id){
        countriesDao.openCurrentSession();
        Countries country=countriesDao.findById(id);
        countriesDao.closeCurrentSession();
        return country;
    }

    public List<Countries> getAll(){
        countriesDao.openCurrentSession();
        List<Countries> countries= countriesDao.getAll();
        countriesDao.closeCurrentSession();
        return countries;
    }

    public List<Countries> getRandomQuestions(){
        countriesDao.openCurrentSession();
        List<Countries> countries=countriesDao.getRandomQuestions();
        countriesDao.closeCurrentSession();
        return countries;
    }

    public CountriesDao countriesDao(){
        return countriesDao;
    }

    public void updatePriorityByName(String name, int value){
        countriesDao.openCurrentSessionWithTransaction();
        countriesDao.updatePriorityByName(name, value);
        countriesDao.closeCurrentSessionWithTransaction();
    }

    public int getPriorityByName(String name){
        countriesDao.openCurrentSession();
        int priority = countriesDao.getPriorityByName(name);
        countriesDao.closeCurrentSession();
        return priority;
    }
}
