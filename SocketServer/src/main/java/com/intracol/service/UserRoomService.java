package com.intracol.service;

import com.intracol.dao.RoomDao;
import com.intracol.dao.UserRoomDao;
import com.intracol.entities.Room;
import com.intracol.entities.User;
import com.intracol.entities.UserRoom;

import java.util.List;

/**
 * Created by Ventsy on 06/07/2017.
 */
public class UserRoomService {

    private static UserRoomDao userRoomDao;

    public UserRoomService(){
        userRoomDao= new UserRoomDao();
    }

    public void insert(UserRoom entity){
        userRoomDao.openCurrentSessionWithTransaction();
        userRoomDao.insert(entity);
        userRoomDao.closeCurrentSessionWithTransaction();
    }

    public void insertRoomAndUserRoom(Room room, UserRoom userRoom) {
        RoomDao roomDao = new RoomDao();
        userRoomDao.openCurrentSessionWithTransaction();
        roomDao.insert(room);
        userRoomDao.insert(userRoom);
        userRoomDao.closeCurrentSessionWithTransaction();
    }

    public void update(UserRoom entity){
        userRoomDao.openCurrentSessionWithTransaction();
        userRoomDao.update(entity);
        userRoomDao.closeCurrentSessionWithTransaction();
    }

    public void delete(UserRoom entity){
        userRoomDao.openCurrentSessionWithTransaction();
        userRoomDao.delete(entity);
        userRoomDao.closeCurrentSessionWithTransaction();
    }

    public List<User> getAllUsersByRoomID(long roomId){
        userRoomDao.openCurrentSession();
        List<User> users= userRoomDao.getAllUsersInRoom(roomId);
        userRoomDao.closeCurrentSession();
        return users;
    }

    public long getPlayerCountForRoom(long roomId){
        userRoomDao.openCurrentSession();
        long userIdCount= userRoomDao.getPlayerCountForRoom(roomId);
        userRoomDao.closeCurrentSession();
        return userIdCount;
    }

    public List<UserRoom> getAllUserRooms(){
        userRoomDao.openCurrentSession();
        List<UserRoom> userRooms = userRoomDao.getAllUserRooms();
        userRoomDao.closeCurrentSession();
        return userRooms;
    }

    public UserRoom findUserRoomByUserId(long userId) {
        userRoomDao.openCurrentSession();
        UserRoom userRoom = userRoomDao.findUserRoomByUserId(userId);
        userRoomDao.closeCurrentSession();
        return userRoom;
    }

    public void deletUserByUserId(UserRoom entity){
        userRoomDao.openCurrentSessionWithTransaction();
        userRoomDao.deletUserByUserId(entity);
        userRoomDao.closeCurrentSessionWithTransaction();
    }
}
