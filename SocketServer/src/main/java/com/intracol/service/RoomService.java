package com.intracol.service;

import com.intracol.dao.RoomDao;
import com.intracol.dao.UsersDao;
import com.intracol.entities.Room;
import com.intracol.entities.User;

import java.util.List;

/**
 * Created by Stefan on 6.7.2017 г..
 */
public class RoomService {
    private static RoomDao roomDAO;

    public RoomService(){
        roomDAO = new RoomDao();
    }

    public void update(Room entity){
        roomDAO.openCurrentSessionWithTransaction();
        roomDAO.update(entity);
        roomDAO.closeCurrentSessionWithTransaction();
    }
    public void insert(Room entity) {
        roomDAO.openCurrentSessionWithTransaction();
        roomDAO.insert(entity);
        roomDAO.closeCurrentSessionWithTransaction();
    }
    public Room findById(long id){
        roomDAO.openCurrentSession();
        Room room = roomDAO.findById(id);
        roomDAO.closeCurrentSession();
        return room;
    }
    public Room findRoomByRoomName(String name){
        roomDAO.openCurrentSession();
        Room room= roomDAO.findByRoomName(name);
        roomDAO.closeCurrentSession();
        return room;
    }
    public void delete(long id){
        Room entity = findById(id);
        roomDAO.openCurrentSessionWithTransaction();
        roomDAO.delete(entity);
        roomDAO.closeCurrentSessionWithTransaction();
    }
    public List<Room> getAll(){
        roomDAO.openCurrentSession();
        List<Room> rooms = roomDAO.getAll();
        roomDAO.closeCurrentSession();
        return rooms;
    }
}
