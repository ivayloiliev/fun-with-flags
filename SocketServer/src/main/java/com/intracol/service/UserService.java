package com.intracol.service;

import com.intracol.dao.UsersDao;
import com.intracol.entities.User;

/**
 * Created by Stefan on 5.7.2017 г..
 */
public class UserService {
    private static UsersDao userDAO;

    public UserService(){
        userDAO = new UsersDao();
    }
    public void update(User entity){
        userDAO.openCurrentSessionWithTransaction();
        userDAO.update(entity);
        userDAO.closeCurrentSessionWithTransaction();
    }
    public void insert(User entity) {
        userDAO.openCurrentSessionWithTransaction();
        userDAO.insert(entity);
        userDAO.closeCurrentSessionWithTransaction();
    }
    public User findById(long id){
        userDAO.openCurrentSession();
        User user = userDAO.findById(id);
        userDAO.closeCurrentSession();
        return user;
    }

    public User findByUsername(String name){
        userDAO.openCurrentSession();
        User user = userDAO.findByUsername(name);
        userDAO.closeCurrentSession();
        return user;
    }

    public void delete(long id){
        userDAO.openCurrentSessionWithTransaction();
        userDAO.delete(id);
        userDAO.closeCurrentSessionWithTransaction();
    }
}
