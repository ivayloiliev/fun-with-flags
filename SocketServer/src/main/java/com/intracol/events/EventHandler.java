package com.intracol.events;

import com.intracol.dao.UserRoomDao;
import com.intracol.entities.*;
import com.intracol.helper.UserSockets;
import com.intracol.service.*;
import org.java_websocket.WebSocket;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.*;

import static com.intracol.Server.FlagSessionServer.hardFixPlayerKick;

/**
 * Created by victor on 06.07.17.
 */
public class EventHandler {

    private UserService userSvc = new UserService();
    private UserRoomService userRoomService = new UserRoomService();
    private RoomService roomService = new RoomService();
    private CountriesService flagCountries = new CountriesService();
    private GameSessionService gameSessionService = new GameSessionService();

    public JSONObject executeEvent(WebSocket conn, Map<String, JSONObject> map) {
        String event = map.keySet().iterator().next();

        switch (event) {
            case "login":
                return loginEvent(conn, map);
            case "getPlayersInRoom":
                return getPlayersInRoom(map);
            case "getAllRooms":
                return getAllRooms(map);

            case "changeUserStatus":
                return changeUserStatus(map);

            case "createRoom":
                return createRoom(map);

            case "getQuestions":
                return getQuestionsEvent(initUsers(map));

            case "joinRoom":
                return joinRoomEvent(map);

            case "leaveRoom":
                return leaveRoomEvent(map);

            case "kickUser":
                return kickUserEvent(map);

            case "register":
                return registerEvent(conn, map);

            case "questionAnswers":
                changeDifficulty(map);
                return updateScore(map);

            case "freeze": return freezeEvent(map);

            default:
                return null;
        }
    }

    private JSONObject leaveRoomEvent(Map<String, JSONObject> map) {
        JSONObject requestData = map.get("leaveRoom");
        Long userId = requestData.getLong("userId");
        UserRoom userRoom = userRoomService.findUserRoomByUserId(userId);
        Long roomId;
        if (userRoom != null) {
            roomId = userRoom.getRoomId();
            userRoomService.delete(userRoom);
            Long playersCount = userRoomService.getPlayerCountForRoom(roomId);
            if (playersCount == 0) {
                roomService.delete(roomId);
            }
            map.get("leaveRoom").put("roomId", roomId);
            Long adminId = roomService.findById(roomId).getAdminId();
            if (userId == adminId) {
                JSONArray users = getPlayersInRoom(map).getJSONArray("roomMembers");
                for (int i = 0; i < users.length(); i ++) {
                    Map<String, JSONObject> kickUserFakeMap = new HashMap<>();
                    kickUserFakeMap.put("kickUser", new JSONObject().put("userId", users.getJSONObject(i).getLong("userId")).put("noRoomRefresh", true));
                    kickUserEvent(kickUserFakeMap);
                }
                return new JSONObject().put("internalInfo", new JSONObject().put("sendType", "NUL"));
            } else
                return getPlayersInRoom(map)
                    .put("internalInfo", new JSONObject().put("sendType", "room").put("roomId", roomId));
        }
        map.get("leaveRoom").put("roomId", 0);
        return getPlayersInRoom(map)
                .put("internalInfo", new JSONObject().put("sendType", "room"));
    }

    private JSONObject createRoom(Map<String, JSONObject> map) {
        JSONObject forUserId = map.get("createRoom");
        User user = userSvc.findById(forUserId.getLong("userId"));
        long userId = user.getUserId();
        Room room = new Room(user.getUsername() + "`s room", userId);
        roomService.insert(room);
        UserRoom userRoom = new UserRoom(userId, room.getId());
        userRoomService.insert(userRoom);
        return new JSONObject().put("internalInfo", new JSONObject().put("sendType", "sender"))
                .put("createRoom", new JSONObject().put("id", roomService.findRoomByRoomName(room.getName()).getId()));
    }

    private JSONObject getAllRooms(Map<String, JSONObject> map) {

        List<Room> rooms = roomService.getAll();
        List<UserRoom> userRooms = userRoomService.getAllUserRooms();
        int i = 0;
        JSONArray array = new JSONArray();
        JSONObject result = new JSONObject();

        for (Room currentRoom : rooms) {
            JSONObject currentObj = new JSONObject();
            currentObj.put("name", currentRoom.getName());
            currentObj.put("id", currentRoom.getId());
            for (UserRoom currnetUserRoom : userRooms) {
                if (currnetUserRoom.getRoomId() == currentRoom.getId()) i++;
            }
            currentObj.put("playersCount", i);
            i = 0;
            array.put(currentObj);
        }
        result.put("room", array);

        return result.put("internalInfo", new JSONObject().put("sendType", map.get("getAllRooms").getString("sendType")));
    }

    private JSONObject loginEvent(WebSocket conn, Map<String, JSONObject> map) {
        JSONObject jsonResult = map.get("login");

        if (!(jsonResult.has("user") && jsonResult.has("pass"))) {
            jsonResult.put("success", false);
            jsonResult.remove("pass");
            return jsonResult;
        }

        String name = (String) jsonResult.get("user");
        String pass = (String) jsonResult.get("pass");
        User user = userSvc.findByUsername(name);

        if (user != null) {
            if (user.getPassword().equals(pass)) {
                jsonResult.put("success", true);
                UserSockets.getUserSocketMapInstance().add(user.getUserId(), conn);
                jsonResult.put("userId", user.getUserId());
            } else {
                jsonResult.put("success", false);
            }
        } else {
            jsonResult.put("success", false);
        }
        jsonResult.remove("pass");
        return new JSONObject().put("login", jsonResult).put("internalInfo", new JSONObject().put("sendType", "sender"));
    }

    private JSONObject getPlayersInRoom(Map<String, JSONObject> map) {
        JSONObject jsObj = map.get(map.keySet().iterator().next());
        JSONArray result = new JSONArray();

        long roomId = jsObj.getLong("roomId");

        if (roomId <= 0) {
            throw new IndexOutOfBoundsException("Room ID must be bigger than 0");
        } else {
            List<User> users = userRoomService.getAllUsersByRoomID(roomId);
            Room room = roomService.findById(roomId);
            JSONArray jasonArr = new JSONArray();
            for (User user : users) {
                JSONObject jo = new JSONObject();
                jo.put("userId", user.getUserId());
                jo.put("userName", user.getUsername());
                if (room.getAdminId() == user.getUserId()) {
                    jo.put("admin", true);
                } else {
                    jo.put("admin", false);
                }
                jasonArr.put(jo);
            }

            JSONObject mainObj = new JSONObject();
            mainObj.put("roomMembers", jasonArr);
            return mainObj.put("internalInfo", new JSONObject().put("sendType", "sender"));
        }
    }

    private JSONObject changeUserStatus(Map<String, JSONObject> map) {
        JSONObject jsObj = map.get("changeUserStatus");

        long userId = jsObj.getLong("userId");
        int status = jsObj.getInt("status");

        UserService service = new UserService();
        User user = service.findById(userId);

        JSONObject obj = new JSONObject();
        obj.put("userId", userId);
        obj.put("user", user.getUsername());
        obj.put("status", status);

        RoomService roomService = new RoomService();

        UserRoomService userRoomService = new UserRoomService();
        UserRoom userRoom = userRoomService.findUserRoomByUserId(userId);


        JSONObject result = new JSONObject();
        result.put("userGameStatus", obj);

        return result.put("internalInfo", new JSONObject().put("sendType", "room").put("roomId", userRoom.getRoomId()));
    }

    private JSONObject getQuestionsEvent(long[] sessionIdAndRoomId) {

        JSONArray jsnArr = new JSONArray();
        JSONObject mainObj = new JSONObject();

        List<Countries> allQuestions = flagCountries.getAll();
        List<Countries> questions = flagCountries.getRandomQuestions();
        List<Integer> notFlag = new ArrayList<>();
        List<String> answers = new ArrayList<String>();

        Random rand = new Random();

        while (notFlag.size() < 3) { //generirame na koi vuprosi shte imame not a flag
            int index = rand.nextInt(10);
            if (!notFlag.contains(index)) {
                notFlag.add(index);
            }
        }

        for (int i = 0; i < 10; i++) {
            JSONObject jsnObj = new JSONObject();

            answers.clear();
            if (notFlag.contains(i)) {
                answers.add("notFlag");
            }

            answers.add(questions.get(i).getCountryCode());

            int k = 0;
            while (answers.size() < 5) { //generirame 5 otgovora,koito ne sa ot generiranite 10 vuprosa
                int index = rand.nextInt(allQuestions.size());
                if (!answers.contains(allQuestions.get(index).getCountryCode())) {
                    answers.add(allQuestions.get(index).getCountryCode());
                    if (!questions.contains(answers.get(k))) {
                        k++;
                    } else {
                        answers.remove(k);
                    }
                }
            }
            Collections.shuffle(answers);

            jsnObj.put("label", questions.get(i).getCountryName());

            for (int j = 1; j < 6; j++) {
                jsnObj.put("a" + j, answers.get(j - 1));
            }
            jsnObj.put("answer", questions.get(i).getCountryCode());
            jsnArr.put(i, jsnObj);
        }

        mainObj.put("questions", jsnArr);
        mainObj.put("sessionId", sessionIdAndRoomId[0]);
        return mainObj.put("internalInfo", new JSONObject().put("sendType", "roomAndSender").put("roomId",sessionIdAndRoomId[1]));
    }

    private JSONObject joinRoomEvent(Map<String, JSONObject> map) {
        JSONObject object = map.get("joinRoom");
        long userId = object.getLong("userId");
        long roomId = object.getLong("roomId");

        if (userRoomService.findUserRoomByUserId(userId) != null)
            userRoomService.delete(userRoomService.findUserRoomByUserId(userId));
        UserRoom userRoom = new UserRoom(userId, roomId);
        userRoomService.insert(userRoom);
        return getPlayersInRoom(map).put("internalInfo", new JSONObject().put("roomId", roomId).put("sendType", "room"));

    }

    private JSONObject kickUserEvent(Map<String, JSONObject> map) {
        JSONObject jsonObject = map.get("kickUser");
        long userId = jsonObject.getLong("userId");
        Long roomId;

        UserRoom userRoom = userRoomService.findUserRoomByUserId(userId);
        if (userRoom != null) {
            roomId = userRoom.getRoomId();
            userRoomService.delete(userRoom);
            Long playersCount = userRoomService.getPlayerCountForRoom(roomId);

            if (playersCount == 0) {
                roomService.delete(roomId);
            }
            map.get("kickUser").put("roomId", roomId);

            if (map.get("kickUser").has("noRoomRefresh"))
                hardFixPlayerKick(userId);

            return getPlayersInRoom(map)
                       .put("internalInfo", new JSONObject().put("roomId", roomId).put("kickUserId", userId).put("sendType", "roomAndKick"));
        }
        else
            return getPlayersInRoom(map)
                       .put("internalInfo", new JSONObject().put("sendType", "roomAndKick"));
    }

    private JSONObject registerEvent(WebSocket conn, Map<String, JSONObject> map) {
        JSONObject jsonFromMap = map.get("register");
        JSONObject result = new JSONObject();

        if (!(jsonFromMap.has("user") && jsonFromMap.has("pass"))) {
            result.put("success", false);
            return result;
        }

        String name = (String) jsonFromMap.get("user");
        String pass = (String) jsonFromMap.get("pass");

        User user = userSvc.findByUsername(name);

        if (user == null) {
            User newUser = new User(name, pass, new Date());
            userSvc.insert(newUser);
            result.put("user", newUser.getUsername());
            result.put("success", true);
            UserSockets.getUserSocketMapInstance().add(newUser.getUserId(), conn);
            result.put("userId", newUser.getUserId());
        } else {
            result.put("success", false);
        }
        return new JSONObject().put("register", result).put("internalInfo", new JSONObject().put("sendType", "sender"));
    }

    private void changeDifficulty(Map<String, JSONObject> map) {
        JSONObject object = map.get("questionAnswers");
        JSONArray arr = (JSONArray) object.get("questionAnswers");

        for (int i = 0; i < arr.length(); i++) {
            JSONObject jsonObject = arr.getJSONObject(i);
            String name = (String) jsonObject.get("label");
            int currentScore = (int) jsonObject.get("score");
            int score = flagCountries.getPriorityByName(name);

            if (score == 100 || score == 1) return;
            else if (currentScore == 0) {
                flagCountries.updatePriorityByName(name, score + 2);
            } else if (currentScore >= 10 && currentScore <= 15) {
                flagCountries.updatePriorityByName(name, score - 1);
            } else if (currentScore > 15 && currentScore <= 20) {
                flagCountries.updatePriorityByName(name, score - 2);
            } else {
                flagCountries.updatePriorityByName(name, score - 3);
            }
        }
    }

    private long[] initUsers(Map<String, JSONObject> map) {
        JSONObject questionsObject = map.get("roomId");
        long roomId = questionsObject.getInt("roomId");

        List<User> users = userRoomService.getAllUsersByRoomID(roomId);
        GameSession gameSession = new GameSession();

        for (int i = 0; i < users.size(); i++) {
            if (i == 0) {
                gameSession.setUser0Id(users.get(i).getUserId());
            } else if (i == 1) {
                gameSession.setUser1Id(users.get(i).getUserId());
            } else if (i == 2) {
                gameSession.setUser2Id(users.get(i).getUserId());
            } else if (i == 3) {
                gameSession.setUser3Id(users.get(i).getUserId());
            } else if (i == 4) {
                gameSession.setUser4Id(users.get(i).getUserId());
            } else if (i == 5) {
                gameSession.setUser5Id(users.get(i).getUserId());
            } else if (i == 6) {
                gameSession.setUser6Id(users.get(i).getUserId());
            } else if (i == 7) {
                gameSession.setUser7Id(users.get(i).getUserId());
            }
        }
        gameSession.setCountUsers(users.size());
        gameSessionService.insert(gameSession);
        long sessionId= gameSessionService.getSessionIdByUser0ID(users.get(0).getUserId());
        long[] result = new long[2];
        result[0] = sessionId;
        result[1] = roomId;
        return result;
    }

    private JSONObject updateScore(Map<String, JSONObject> map) {
        JSONObject questionsObject = map.get("questionAnswers");
        JSONObject userIdObject = map.get("userId");
        JSONObject sessionIdObject = map.get("sessionId");
        int sessionId = sessionIdObject.getInt("sessionId");
        int userId = userIdObject.getInt("userId");
        JSONArray arr = (JSONArray) questionsObject.get("questionAnswers");
        int score = 0;
        for (int i = 0; i < arr.length(); i++) {
            JSONObject jsonObject = arr.getJSONObject(i);
            score += jsonObject.getInt("score");
        }
        User user = userSvc.findById(userId);
        user.setUserPoints(user.getUserPoints() + score);
        userSvc.update(user);

        GameSession gameSession = gameSessionService.findGameSessionById(sessionId);
        if (gameSession.getUser0Id() == userId) {
            gameSession.setScore0(score);
        } else if (gameSession.getUser1Id() == userId) {
            gameSession.setScore1(score);
        } else if (gameSession.getUser2Id() == userId) {
            gameSession.setScore2(score);
        } else if (gameSession.getUser3Id() == userId) {
            gameSession.setScore3(score);
        } else if (gameSession.getUser4Id() == userId) {
            gameSession.setScore4(score);
        } else if (gameSession.getUser5Id() == userId) {
            gameSession.setScore5(score);
        } else if (gameSession.getUser6Id() == userId) {
            gameSession.setScore6(score);
        } else if (gameSession.getUser7Id() == userId) {
            gameSession.setScore7(score);
        }
        gameSession.setCountUsers(gameSession.getCountUsers() - 1);
        gameSessionService.update(gameSession);
        if (gameSession.getCountUsers() <= 0) {
            return endGameSession(sessionId);
        }
        return new JSONObject().put("internalInfo", new JSONObject().put("sendType", "roomAndSender"));
    }

    private JSONObject endGameSession(int sessionId) {
        JSONObject mainObj = new JSONObject();
        JSONArray array = new JSONArray();
        JSONObject secondObj = null;
        GameSession gameSess = gameSessionService.findGameSessionById(sessionId);
        if (gameSess.getUser0Id() != 0) {
            secondObj = new JSONObject();
            secondObj.put("userId", gameSess.getUser0Id());
            secondObj.put("user", userSvc.findById(gameSess.getUser0Id()).getUsername());
            secondObj.put("score", gameSess.getScore0());
            array.put(secondObj);
        }
        if (gameSess.getUser1Id() != 0) {
            secondObj = new JSONObject();
            secondObj.put("userId", gameSess.getUser1Id());
            secondObj.put("user", userSvc.findById(gameSess.getUser1Id()).getUsername());
            secondObj.put("score", gameSess.getScore1());
            array.put(secondObj);
        }
        if (gameSess.getUser2Id() != 0) {
            secondObj = new JSONObject();
            secondObj.put("userId", gameSess.getUser2Id());
            secondObj.put("user", userSvc.findById(gameSess.getUser2Id()).getUsername());
            secondObj.put("score", gameSess.getScore2());
            array.put(secondObj);
        }
        if (gameSess.getUser3Id() != 0) {
            secondObj = new JSONObject();
            secondObj.put("userId", gameSess.getUser3Id());
            secondObj.put("user", userSvc.findById(gameSess.getUser3Id()).getUsername());
            secondObj.put("score", gameSess.getScore3());
            array.put(secondObj);
        }
        if (gameSess.getUser4Id() != 0) {
            secondObj = new JSONObject();
            secondObj.put("userId", gameSess.getUser4Id());
            secondObj.put("user", userSvc.findById(gameSess.getUser4Id()).getUsername());
            secondObj.put("score", gameSess.getScore4());
            array.put(secondObj);
        }
        if (gameSess.getUser5Id() != 0) {
            secondObj = new JSONObject();
            secondObj.put("userId", gameSess.getUser5Id());
            secondObj.put("user", userSvc.findById(gameSess.getUser5Id()).getUsername());
            secondObj.put("score", gameSess.getScore5());
            array.put(secondObj);
        }
        if (gameSess.getUser6Id() != 0) {
            secondObj = new JSONObject();
            secondObj.put("userId", gameSess.getUser6Id());
            secondObj.put("user", userSvc.findById(gameSess.getUser6Id()).getUsername());
            secondObj.put("score", gameSess.getScore6());
            array.put(secondObj);
        }
        if (gameSess.getUser7Id() != 0) {
            secondObj = new JSONObject();
            secondObj.put("userId", gameSess.getUser7Id());
            secondObj.put("user", userSvc.findById(gameSess.getUser7Id()).getUsername());
            secondObj.put("score", gameSess.getScore7());
            array.put(secondObj);
        }
        mainObj.put("endGame", array);
        gameSessionService.delete(gameSess);
        long roomId = userRoomService.findUserRoomByUserId(gameSess.getUser0Id()).getRoomId();
        return mainObj.put("internalInfo", new JSONObject().put("sendType", "roomAndSender").put("roomId",roomId));
    }

    private JSONObject freezeEvent(Map<String, JSONObject> map) {
        JSONObject forRoomId = map.get("freeze");
        long roomId = forRoomId.getLong("roomId");
        JSONObject mainObj = new JSONObject().put("freeze", true);
        //mainObj.put("roomId",roomId);
        return mainObj.put("internalInfo",new JSONObject().put("sendType", "room").put("roomId",roomId));
    }
}
